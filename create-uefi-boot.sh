#!/bin/bash
#
# Simple usb boot media creator
#
# Author : Per Hallsmark <per@hallsmark.se>
#
#
# Creates a usb drive (well could be other types of media too, usb is the one
# I use though) with two partitions :
# * first is a uefi/bios boot (vfat) of 1GB, large enough for a large initrd
#   if needed
# * second is a ext4 rootfs
#

DEV=/dev/sdb

TMP=$(mktemp -d)

IMGDIR=phallsma@arn-build3.wrs.com:/opt/phallsma/wrl-rcs-snr/build/build/tmp-glibc/deploy/images/axxiax86-64
KERNEL=bzImage
ROOTFS=wrlinux-image-glibc-std-axxiax86-64.tar.bz2
INITRD=prime-snr-initramfs-axxiax86-64.cpio.gz

clean_dev()
{
	sgdisk --zap-all $1
}

create_boot_partition()
{
	sgdisk --new=1:0:+1G --typecode=0:0700 --attributes 0:set:2 $1
	mkfs.vfat -F32 -n BOOT ${1}1
}

create_rootfs_partition()
{
	sgdisk --largest-new=0 --typecode=0:8300 $1
	mkfs.ext4 -F -L rootfs ${1}2
}

deploy_boot_fs()
{
	mkdir ${TMP}/boot
	mount $1 ${TMP}/boot
	cp -r ${*:2} ${TMP}/boot
	sync
	umount ${TMP}/boot
}

deploy_root_fs()
{
	mkdir ${TMP}/root
	mount $1 ${TMP}/root
	tar -C ${TMP}/root -xf $2
	if [ "$#" -gt 2 ]; then
		cp -r ${*:3} ${TMP}/root
	fi
	sync
	umount ${TMP}/root
}

clean_dev ${DEV}

create_boot_partition ${DEV}
create_rootfs_partition ${DEV}

scp ${IMGDIR}/${KERNEL} ${TMP}/bzImage
scp ${IMGDIR}/${INITRD} ${TMP}/initrd.gz
scp ${IMGDIR}/${ROOTFS} ${TMP}/rootfs.tar.gz

deploy_boot_fs ${DEV}1 efi/* ${TMP}/bzImage ${TMP}/initrd.gz
deploy_root_fs ${DEV}2 ${TMP}/rootfs.tar.gz root

rm -rf ${TMP}
